# Auto Typing Text JS

This is my simple project which makes a div or element type its own text.


## How to use it?
1. Download the 'type-my.js' file or 'type-my.min.js'
2. Then declare the TypeMe class
```
const tm = new TypeMy('myId')
tm.run()
```

## Feature
### 1. Typing Speed
By default the typing speed is 300 milliseconds. You can change it by:
```
tm.speed = speed_you_want_in_milisecond
```

### 2. Emoji
By default the typing mark at the end of the sentence is the pipe symbol. You can change it by:
```
tm.emoji = "👆"
```
If you dont want it, dont set with empty string. Use this instead:
```
tm.showEmoji = false
```
You also can make the emoji blink.
```
tm.emojiBlink = true
tm.emojiBlinkSpeed = 1000
```

class TypeMy {
	#land
	#landEmoji
	#theText
	#parentId
	emoji = '|'
	showEmoji = true
	emojiBlink = false
	emojiBlinkSpeed = 1000
	speed = 300
	#whereTheText = 0
	#textLength

	// id of the element and speed in milisecond
	constructor(id, speed = 300) {
		this.#land = document.getElementById(id)
		this.#theText = this.#land.innerHTML
		this.#textLength = this.#theText.length
		this.#parentId = id
		this.speed = speed
	}

	run() {
		// split the div into 2 parts.
		// for the text
		this.#land.innerHTML = `<span id="${this.#parentId}_typeMe_text"></span>`

		// emoji
		if (this.showEmoji) {
			this.#land.innerHTML += `<span id="${this.#parentId}_typeMe_emoji">${this.emoji}</span>`
			this.#landEmoji = document.getElementById(this.#parentId+'_typeMe_emoji')

			if (this.emojiBlink) {
				this.#landEmoji.style.animation = `typeMy_blink ${this.emojiBlinkSpeed}ms infinite`
			}
			
		}

		this.#land = document.getElementById(this.#parentId+'_typeMe_text')

		// run typing
		this.#typeMe()
	}

	#typeMe() {
		this.#land.append(this.#theText[this.#whereTheText])
		this.#whereTheText++
		var that = this
		if (this.#whereTheText < this.#textLength) {
			setTimeout(() => {
				that.#typeMe()
			}, this.speed)
		} else {
			this.#landEmoji.innerHTML = ""
		}
	}
}